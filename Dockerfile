FROM golang

##RUN mkdir -p /usr/src/app/
##WORKDIR /usr/src/app/

##COPY . /usr/src/app/

##EXPOSE 8080
##ENTRYPOINT ["sh", "/usr/src/app/"]


ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
COPY . /app
WORKDIR /app

RUN go mod download
RUN chmod +x /app/cmd/web
RUN go build /app/cmd/web

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app .
EXPOSE 4000
ENTRYPOINT ./web
